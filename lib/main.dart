 import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget headerSection = Container(
      padding: EdgeInsets.all(12),
      child: Center(
        child: Stack(
          children: [
            Text('Nutchaya Pintabut',
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
                foreground: Paint()
                  ..style = PaintingStyle.stroke
                  ..strokeWidth = 3
                  ..color = Colors.yellow[700]!
              ),
            ),
            Text('Nutchaya Pintabut',
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
                color: Colors.black
              ),
            ),
          ],
        )
      ),
    );
    Widget profileSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTopic('Profile'),
          Table(
            children: [
              _buildinfo('Name', 'Nutchaya Pintabut'),
              _buildinfo('Nickname', 'Nut'),
              _buildinfo('Birthday', '20 Jul 2000'),
              _buildinfo('Age', '21 years'),
              _buildinfo('Gender', 'Female'),
            ],)
        ],
      ),
    );
    Widget contactSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTopic('Contact'),
          _buildContactInfo(Icons.call, '096-890-1732'),
          _buildContactInfo(Icons.email, '61160140@go.buu.ac.th'),
        ],
      ),
    );
    Widget skillsSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTopic('Skills'),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 30),
            height: 80,
            child: new ListView(
              scrollDirection: Axis.horizontal,
              children: [
                _buildskill('images/java.png', 'Java'),
                _buildskill('images/python.png', 'Python'),
                _buildskill('images/html5.png', 'HTML5'),
                _buildskill('images/css.png', 'CSS'),
                _buildskill('images/js-logo.png', 'Javascript'),
                _buildskill('images/bootstrap.png', 'Bootstrap'),
                _buildskill('images/nodejs.png', 'Node.js'),
                _buildskill('images/vuejs.png', 'Vue.js'),
                _buildskill('images/swift.png', 'wift'),
                _buildskill('images/flutter.png', 'Flutter'),
                _buildskill('images/mysql.png', 'MySQL'),
                _buildskill('images/mongoDB.png', 'MongoDB'),
              ],
            )
          )
        ],
      ),
    );
    Widget languagesSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTopic('Language'),
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(top: 10,left: 30),
                child: Text('TH',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 10,left: 30),
                child: Text('EN',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
    Widget educationSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTopic('Education'),
          Table(
            children: [
              _buildEduInfo('2018 - now', 'Bachelor of Science (Computer Science), Faculty of informatics', 'Burapha University, Chonburi'),
              _buildEduInfo('2012 - 2018', 'IETC (Intensive English and Technology Class)', 'Satrinonthaburi School, Nonthaburi'),
            ],
          )
        ],
      ),
    );
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Nutchaya\'s Resume'),
          backgroundColor: Colors.black,
        ),
        body: ListView(
          children: [
            headerSection,
            Image.asset('images/Profile.jpg', 
             height: 400,),
             profileSection,
             contactSection,
             skillsSection,
             languagesSection,
             educationSection,
          ],
        ),
        
        ),
      
    );
  }
}

Container _buildTopic(String label){
  return Container(
    padding: EdgeInsets.only(left: 30, top: 20),
    child: Text(label,
      style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 25,
          decoration: TextDecoration.underline,
          decorationColor: Colors.yellow[800],
        ),
    ),
  );
}

TableRow _buildinfo (String topic, String detail){
  return TableRow(
    children: [
      Container(
        padding: EdgeInsets.only(top: 10, left: 30),
        child: Text(topic,
        style: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold
          ),
        ),
      ),
      Container(
        padding: EdgeInsets.only(top: 10, left: 30),
        child: Text(detail,
        style: TextStyle(
        fontSize: 18,
          ),
        ),
      ),
    ],
  );
}

Row _buildContactInfo(IconData icon,String detail){
  return Row(
    children: [
      Container(
        padding: EdgeInsets.only(top: 10, left: 30),
        child: Icon(icon),
      ),
      Container(
        padding: EdgeInsets.only(top: 10, left: 30),
        child: Text(detail,
          style: TextStyle(
            fontSize: 18
          ),
        ),
      )
    ],
  );
}

Container _buildskill(String path,String label){
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 15),
    child: Column(
      children: [
        Image.asset(path,height: 50,),
        Text(label,
        style: TextStyle(
          fontSize:15,
          fontWeight: FontWeight.bold
          ),
        )
      ],
    )
  );
}

TableRow _buildEduInfo(String year, String faculty, String school){
  return TableRow(
    children: [
      Container(
        padding: EdgeInsets.only(top: 10,left: 30),
        child: Text(year,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold
          ),
        ),
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top:10),
            child: Text(faculty,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
          Container(
            child: Text(school,
              style: TextStyle(
                fontSize: 18,
              ),  
            ),
          ),
        ],
      )
    ]
  );
}